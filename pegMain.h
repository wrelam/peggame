/*******************************************************************************
 *
 * pegMain.h
 *
 * Function prototypes, macro definitions, and globals.
*******************************************************************************/
/* Defines */
#define MOVE_BAD                    (-1)
#define MOVE_GOOD                   (1)
#define LOSE                        (-2)
#define WIN                         (2)

/* Function prototypes */
void
play(unsigned short int *board);

signed int
checkForWin(unsigned short int board);

void
resetGame(unsigned short int *board);

signed int
move(unsigned short int *board, signed int src, signed int dst);

void
addPeg(unsigned short int *board, signed int slot);

void
removePeg(unsigned short int *board, signed int slot);

signed int
getSlot(unsigned short int *board, signed int slot);

void
printBoard(const unsigned short int board);

void
printDirections(void);

/* Globals */
unsigned short int g_moves[15] = {    0x0028, 0x0140, 0x0280, 0x1421, 0x2800,
                                      0x5009, 0x0102, 0x0204, 0x0042, 0x0084, 
                                      0x1008, 0x2010, 0x4428, 0x0810, 0x1020 };

unsigned short int g_adjacent[15] = { 0x0006, 0x001D, 0x0033, 0x00D2, 0x01AE,
                                      0x0314, 0x0C88, 0x1958, 0x32B0, 0x6120,
                                      0x0840, 0x14C0, 0x2980, 0x5300, 0x2200 };

