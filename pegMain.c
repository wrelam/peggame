/*******************************************************************************
 *
 * pegMain.c
 *
 * Runs the peg solitaire game
 *
 * This version of peg solitaire is based on the triangular board which has 15
 * pegs, where one is removed to start the game. The object is to hop one peg
 * over another until you can longer make any moves. You win if there is only
 * one peg left.
 *
 * @note    All slot numbers are 0 based, where the 0th slot is at the top of
 *          the board. 
 *
*******************************************************************************/
#include <stdio.h>


/******************************************************************************
 * main
 *
 * Runs the peg game
 *
*******************************************************************************/
signed int
main(void)
{
    unsigned short int board = 0x7FFF;

    printf("Welcome to PegSolitaire\n\n");

    play(&board);

    printf("Thanks for playing!\n");

    return 0;
}

/******************************************************************************
 * play
 *
 * @brief   Allows a player to play the pegGame
 *
 * @param   *board  Representation of the triangular peg board
******************************************************************************/
void
play(unsigned short int *board)
{
    signed char     action = 'h';
    signed char     skip = '';
    unsigned int    src = 0;
    unsigned int    dst = 0;
    signed int      ch = 0;

    printDirections();
    removePeg(board, 7);
    printBoard(*board);

    while('q' != action)
    {
        /* Get user input */
        scanf("%c", &action);

        switch(action)
        {
            case 'q':
                break;
            case 'r':
                resetGame(board);
                break;
            case 'm':
                printf("Enter move (source, destination)\nex. 1, 4\n");
                scanf("%d, %d", &src, &dst);

                if (MOVE_GOOD != move(board, src-1, dst-1))
                {
                    printf("Invalid move\n");
                }
                
                printBoard(*board);

                if (WIN == checkForWin(*board))
                {
                    printf("You won! Congratulations!\n");
                    resetGame(board);
                }

                break;
            /* Intentional */
            case 'h':
            default:
                printDirections();
        }

		/* Skip over everything else in input */
		do
		{
			scanf("%c", &skip);
		}
		while (('\n' != skip) && (EOF != skip));

    }/* End of while loop */
}

/******************************************************************************
 * checkForWin
 *
 * @brief   Checks to see if only one peg is left on the board
 *
 * @param   *board  Representation of the triangular peg board
******************************************************************************/
signed int
checkForWin(unsigned short int board)
{
    switch(board)
    {
        case 1:
        case 2:
        case 4:
        case 8:
        case 16:
        case 32:
        case 64:
        case 128:
        case 256:
        case 512:
        case 1024:
        case 2048:
        case 4096:
        case 8192:
        case 16384:
        case 32768:
            return WIN;
        default:
            return LOSE;
    }
}

/******************************************************************************
 * resetGame
 *
 * @brief   Resets the game board, and removes a random peg
 *
 * @param   *board  Representation of the triangular peg board
******************************************************************************/
void
resetGame(unsigned short int *board)
{
    printf("Resetting board\n");
    *board = 0x7FFF;
    removePeg(board, 3);
    printDirections();
    printBoard(*board);
}

/******************************************************************************
 * move
 *
 * @brief   Attempts to move a peg from the src slot to the dst slot
 *
 * @param   *board  Representation of the triangular peg board
 * @param   src     Peg to move
 * @param   dst     Slot to place peg in
******************************************************************************/
signed int
move(unsigned short int *board, signed int src, signed int dst)
{
    signed toJump;
    unsigned short int jumpBoard;

    /* Error checking */
    if ((NULL == board) ||
        (src == dst)    ||
        (src < 0)       ||
        (src > 15)      ||
        (dst < 0)       ||
        (dst > 15))
    {
        return MOVE_BAD;
    }

    /* Check that this is a valid move */
    if (0 == getSlot(&g_moves[src], dst))
    {
        return MOVE_BAD;
    }

    /* Result of &ing the adjacent pegs to each slot should result in a 1 
     * at the slot number to be jumped over */
    jumpBoard = g_adjacent[src] & g_adjacent[dst];

    /* Cannot jump from src to dst */
    if (0 == jumpBoard)
    {
        return MOVE_BAD;
    }

    /* Determine slot to be jumped */
    for (toJump = 0; toJump <= 15; toJump++)
    {
        if (1 == (1 & jumpBoard))
        {
            break;
        }
        else
        {
            jumpBoard >>= 1;
        }
    }

    /* Check slots */
    if ((0 == getSlot(board, src)) ||
        (1 == getSlot(board, dst)) ||
        (0 == getSlot(board, toJump)))
    {
        return MOVE_BAD;
    }
    else
    {
        removePeg(board, src);
        addPeg(board, dst);
        removePeg(board, toJump);
    }

    return MOVE_GOOD;
}

/******************************************************************************
 * addPeg
 *
 * @brief   Adds a peg to the board at the designated slot
 *
 * @param   *board      Representation of the triangular peg board
 * @param   bitToGet    The desired slot to put a peg in
******************************************************************************/
void
addPeg(unsigned short int *board, signed int slot)
{
    if ((NULL != board) &&
        (15 > slot) &&
        (0 <= slot))
    {
        *board |= (1 << slot);
    }
}

/******************************************************************************
 * removePeg
 *
 * @brief   Removes a peg from the board at the designated slot
 *
 * @param   *board      Representation of the triangular peg board
 * @param   bitToGet    The desired slot to remove a peg from
******************************************************************************/
void
removePeg(unsigned short int *board, signed int slot)
{
    if ((NULL != board) &&
        (15 > slot) &&
        (0 <= slot))
    {
        *board ^= (1 << slot);
    }
}

/******************************************************************************
 * getSlot
 *
 * @brief   Returns the desired slot from the board. The 0th position is the 
 *          least significant bit which corresponds to the top slot
 *
 * @param   board       Representation of the triangular peg board
 * @param   bitToGet    The desired slot to be returned
 *
 * @return  The value of the bit at position (slot) in (board)
******************************************************************************/
signed int
getSlot(unsigned short int *board, signed int slot)
{
    if ((NULL != board) &&
        (15 > slot) &&
        (0 <= slot))
    {
        return (*board >> slot) & 1;
    }
}

/******************************************************************************
 * printBoard
 *
 * @board   Prints out the peg board.
 *
 * @param   board       Representation of the triangular peg board
******************************************************************************/
void
printBoard(const unsigned short int board)
{
    printf("             %d             \n\n",       board       & 1); 
    printf("          %d     %d          \n\n",     (board >> 1) & 1, 
                                                    (board >> 2) & 1); 
    printf("       %d     %d     %d       \n\n",    (board >> 3) & 1,
                                                    (board >> 4) & 1,
                                                    (board >> 5) & 1);
    printf("    %d     %d     %d     %d    \n\n",   (board >> 6) & 1,
                                                    (board >> 7) & 1,
                                                    (board >> 8) & 1,
                                                    (board >> 9) & 1);
    printf(" %d     %d     %d     %d     %d \n\n",  (board >> 10) & 1,
                                                    (board >> 11) & 1,
                                                    (board >> 12) & 1,
                                                    (board >> 13) & 1,
                                                    (board >> 14) & 1);
}

/******************************************************************************
 * printDirections
 *
 * @brief   Prints out directions for playing the game
 *
 * @param   *board  Representation of the triangular peg board
 * @param   src     Peg to move
 * @param   dst     Slot to place peg in
******************************************************************************/
void
printDirections(void)
{
    printf("m: move pegs\n");
    printf("q: quit\n");
    printf("r: reset game\n");
    printf("h: print this menu\n");
}

